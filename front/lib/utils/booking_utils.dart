import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:qr_app/data/key_info.dart';

class BookingBoxUtils {
  static List<BookingUnit> listAllBookings(Box<BookingUnit> box) {
    return box.values.toList();
  }

  static Map<DateTime, List<BookingUnit>> listBookedDaysByRoom(
      Box<BookingUnit> box, String room) {
    Map<DateTime, List<BookingUnit>> result = {};
    for (var element in box.values) {
      if (element.room == room &&
          !result.containsKey(DateUtils.dateOnly(element.dateTime))) {
        List<BookingUnit> parsPerDate = [];
        for (var elem in box.values) {
          if (DateUtils.dateOnly(elem.dateTime) ==
                  DateUtils.dateOnly(element.dateTime) &&
              elem.room == element.room) {
            parsPerDate.add(elem);
          }
        }
        result[element.dateTime] = parsPerDate;
      }
    }
    return result;
  }

  static Map<DateTime, List<BookingUnit>> listBookedRoomsByDay(
      Box<BookingUnit> box, DateTime day) {
    Map<DateTime, List<BookingUnit>> result = {};
    for (var element in box.values) {
      if (DateUtils.dateOnly(element.dateTime) == DateUtils.dateOnly(day)) {
        List<BookingUnit> parsPerDate = [];
        for (var elem in box.values) {
          if (elem.dateTime == element.dateTime) {
            parsPerDate.add(elem);
          }
        }
        result[element.dateTime] = parsPerDate;
      }
    }
    return result;
  }
}

import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:qr_app/data/key_info.dart';
import 'package:qr_app/pages/camera_page.dart';
import 'package:qr_app/pages/room_info_page.dart';

import 'consts.dart' as consts;
import 'pages/login_page.dart';
import 'pages/main_page/main_page.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(BookingUnitAdapter());
  await Hive.openBox<BookingUnit>('booking');
  initializeDateFormatting().then((_) => runApp(MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'QR app',
      theme: ThemeData(
        primarySwatch: consts.materialMisisBlue,
      ),
      debugShowCheckedModeBanner: false,
      routes: {
        '/LoginPage': (BuildContext context) => const LoginPage(),
        '/MainPage': (BuildContext context) => const MainPage(),
        '/RoomInfoPage': (BuildContext context) => RoomInfoPage(),
        '/CameraPage': (BuildContext context) => CameraPage()
      },
      initialRoute: '/MainPage',
    );
  }
}

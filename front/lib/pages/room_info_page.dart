import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_app/data/key_info.dart';
import '../consts.dart' as consts;

class RoomInfoPage extends StatefulWidget {
  @override
  State<RoomInfoPage> createState() => _RoomInfoPageState();
}

class _RoomInfoPageState extends State<RoomInfoPage> {
  @override
  Widget build(BuildContext context) {
    final bookingUnit = ModalRoute.of(context)?.settings.arguments as BookingUnit; ///TODO: change struct
    return Scaffold(
      appBar: AppBar(
        title: Text(bookingUnit.room),
        backgroundColor: consts.misisWhite,
        centerTitle: true,
        elevation: 0,
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () async{
                await bookingUnit.delete().then((value) => Navigator.pop(context));
              },
              child: Text('Отменить бронь'),
            )
          ],
        ),
      )
    );
  }
}
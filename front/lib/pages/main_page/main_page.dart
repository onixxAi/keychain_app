import 'package:flutter/material.dart';
import 'package:qr_app/widgets/navigation_bar.dart';
import '../../consts.dart' as consts;

import 'main_screen.dart';
import 'profile_screen.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);


  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  void changeScreen() {

  }

  void onNavigationBarTap(int val) {
    i = val;
    setState(() {});
  }

  void onNavigationBarButtonPressed() {
    Navigator.pushNamed(context, '/CameraPage');
  }

  int i = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: consts.misisWhite,
      appBar: AppBar(
        backgroundColor: consts.misisWhite,
        title: Text('Главная', style: TextStyle(color: consts.misisBlack),),
        centerTitle: true,
        elevation: 0,
      ),
      body:i == 0? MainScreen():ProfileScreen(),
      bottomNavigationBar: NavigationBars(
        items: [Icons.calendar_today_sharp, Icons.qr_code_scanner, Icons.account_box_sharp],
        onPressed: onNavigationBarButtonPressed,
        onTap: onNavigationBarTap,
        currentIndex: i
      )
    );
  }
}

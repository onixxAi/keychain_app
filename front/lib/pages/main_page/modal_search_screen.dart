import 'package:custom_sliding_segmented_control/custom_sliding_segmented_control.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:qr_app/data/key_info.dart';
import 'package:qr_app/pages/main_page/alertdialog_calendar.dart';
import '../../consts.dart' as consts;

class ModalSearchPage extends StatefulWidget {
  const ModalSearchPage({Key? key}) : super(key: key);

  @override
  State<ModalSearchPage> createState() => _ModalSearchPageState();
}

class _ModalSearchPageState extends State<ModalSearchPage> {
  String dropdownValue = 'Г-511';
  List<DropdownMenuItem<String>> dropdownItems = <String>[
    'Г-511',
    'Б-3',
    'Б-8хх',
    'Б-903',
    'Л-476',
    'В-69'
  ].map<DropdownMenuItem<String>>((String value) {
    return DropdownMenuItem<String>(
      value: value,
      child: Text(value),
    );
  }).toList();

  bool isTouched = false;
  Widget getByNumberScreen() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        DropdownButton<String>(
            value: dropdownValue,
            items: dropdownItems,
            onChanged: (String? newValue) {
              setState(() {
                dropdownValue = newValue!;
              });
            }),
        TextButton(
            onPressed: () {
              alertCalendarController.roomName = dropdownValue;
              showMyDialog();
              isTouched = true;
            },
            child: const Text('Выбор даты'))
      ],
    );
  }

  Widget getByDatetimeScreen() {
    return Row(
      children: [
        DropdownButton<String>(
            items: dropdownItems,
            onChanged: (String? newValue) {
              setState(() {
                dropdownValue = newValue!;
              });
            })
      ],
    );
  }

  AlertCalendarController alertCalendarController =
      AlertCalendarController(dateTime: DateTime.now());
  Future<void> showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Выберите день'),
          content: AlertDialogCalendar(alertCalendarController),
          actions: <Widget>[
            TextButton(
              child: const Text('Готово'),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  lessons = alertCalendarController.bookingUnits;
                });
              },
            ),
          ],
        );
      },
    );
  }

  int currentTab = 0;

  List<BookingUnit> lessons = [];
  List<bool> choosedLessons = List.filled(6, false);

  void onBooked() {
    var box = Hive.box<BookingUnit>('booking');

    for (int i = 0; i < choosedLessons.length; i++) {
      if (choosedLessons[i]) {
        box.add(BookingUnit(alertCalendarController.roomName,
            DateUtils.dateOnly(alertCalendarController.dateTime), i));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      heightFactor: 0.9,
      child: Container(
        color: Colors.transparent,
        child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10.0, // has the effect of softening the shadow
                  spreadRadius: 0.0, // has the effect of extending the shadow
                )
              ],
            ),
            alignment: Alignment.topLeft,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(top: 5, left: 10),
                            child: const Text(
                              "Забронировать",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w900,
                                  color: Colors.black87),
                            ),
                          ),
                          Container(
                              margin: const EdgeInsets.only(top: 5, right: 5),
                              child: FlatButton(
                                padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: const Text(
                                  "Отмена",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: consts.misisRed,
                                  ),
                                ),
                              )),
                        ],
                      ),
                      const SizedBox(height: 5),
                      CustomSlidingSegmentedControl<int>(
                        children: const <int, Widget>{
                          0: Text("По номеру аудитории"),
                          1: Text("По дате-времени")
                        },
                        onValueChanged: (int v) {
                          setState(() {
                            currentTab = v;
                          });
                        },
                      ),
                      currentTab == 0
                          ? getByNumberScreen()
                          : getByDatetimeScreen(),
                      const Divider(),
                      isTouched
                          ? Expanded(
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: 6,
                                  itemBuilder: (context, i) {
                                    return Expanded(
                                        child: Container(
                                      color: lessons
                                              .where((element) =>
                                                  element.para == i)
                                              .isNotEmpty
                                          ? Colors.grey
                                          : choosedLessons[i]
                                              ? Colors.green
                                              : Colors.white,
                                      child: ListTile(
                                          title: Text(
                                            dropdownValue,
                                          ),
                                          subtitle: Text('Пара № ${i + 1}'),
                                          onTap: () {
                                            if (lessons
                                                .where((element) =>
                                                    element.para == i)
                                                .isEmpty) {
                                              setState(() {
                                                choosedLessons[i] =
                                                    !choosedLessons[i];
                                              });
                                            }
                                          }),
                                    ));
                                  }))
                          : SizedBox.shrink()
                    ],
                  ),
                ),
                choosedLessons.where((element) => element).isNotEmpty
                    ? Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          width: double.maxFinite,
                          height: 100,
                          child: ElevatedButton(
                            onPressed: () {
                              onBooked();
                              Navigator.pop(context);
                            },
                            child: Text('Забронировать'),
                          ),
                        ),
                      )
                    : SizedBox.shrink()
              ],
            )),
      ),
    );
  }
}

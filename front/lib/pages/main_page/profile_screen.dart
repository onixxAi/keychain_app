import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:qr_app/data/key_info.dart';
import 'package:qr_app/data/start_end_time.dart';
import 'package:qr_app/widgets/keys_list.dart';
import 'package:qr_app/consts.dart' as consts;

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  void onExit() {
    Navigator.pushReplacementNamed(context, '/LoginPage');
  }

  void onDebug() {
    var box = Hive.box<BookingUnit>('booking');
    box.deleteAll(box.keys);
    box.add(
        BookingUnit('Г-511', DateUtils.dateOnly(DateTime.utc(2021, 12, 8)), 1));
    box.add(
        BookingUnit('Г-511', DateUtils.dateOnly(DateTime.utc(2021, 12, 8)), 2));
    box.add(BookingUnit(
        'Г-511', DateUtils.dateOnly(DateTime.utc(2021, 12, 25)), 3));
    box.add(
        BookingUnit('Б-3', DateUtils.dateOnly(DateTime.utc(2021, 12, 13)), 3));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Альхеев Геннадий Евгеньевич',
              style: consts.misisHeaderBlack36Pt),
          const SizedBox(
            height: 10,
          ),
          SizedBox(
            height: 50,
            child: Stack(
              children: [
                Align(
                    alignment: Alignment.centerRight,
                    child: ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: [
                        Container(
                          width: 100,
                          height: 40,
                          color: const Color(0xFFF22929),
                          child: TextButton(
                            onPressed: onExit,
                            child: const Text('Выйти',
                                style: consts.misisHeaderWhite18Pt),
                          ),
                        ),
                        Container(
                          width: 100,
                          height: 40,
                          color: const Color(0xFF00D21B),
                          child: TextButton(
                            onPressed: onDebug,
                            child: const Text('Init DB',
                                style: consts.misisHeaderWhite18Pt),
                          ),
                        ),
                      ],
                    )),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      child: const Text(
                        'm2007379@edu.misis.ru',
                        overflow: TextOverflow.fade,
                        maxLines: 1,
                        softWrap: false,
                        style: consts.misisTextBlack14Pt,
                      ),
                    ))
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Divider(),
          const SizedBox(
            height: 20,
          ),
          Text('Ключи', style: consts.misisHeaderBlack36Pt),
          const SizedBox(
            height: 30,
          ),
          KeysList(
            keys: [BookingUnit('г-511', DateTime.now(), 2)],
            textElement: InteractiveElement(
                text: 'Передать',
                onPressed: (int i) {},
                color: consts.misisGreen),
          ),
          Divider(),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:qr_app/data/key_info.dart';
import 'package:qr_app/utils/booking_utils.dart';
import '../../consts.dart' as consts;
import 'package:table_calendar/table_calendar.dart';

import 'modal_search_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);


  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  DateTime _focusedDay = DateUtils.dateOnly(DateTime.now());

  int segmentedControlGroupValue = 0;

  Map<DateTime, List<BookingUnit>> events = {};
  void getLessonsToday() {
    var box = Hive.box<BookingUnit>('booking');
    events = BookingBoxUtils.listBookedRoomsByDay(box, DateUtils.dateOnly(_focusedDay));

    for (var k in events.keys) {
      events[k]!.sort((a,b) => a.para.compareTo(b.para));
    }
  }

  void _showBottomModal(context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        builder: (builder) {
          return ModalSearchPage();
        }).then((value) => {
          setState(() {getLessonsToday();})});
    }


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: TableCalendar(
                  locale: 'ru_RU',
                  headerStyle: const HeaderStyle(formatButtonVisible: false),
                  firstDay: DateTime.now(),
                  lastDay: DateTime.utc(2030, 3, 14),
                  currentDay: _focusedDay,
                  focusedDay: DateTime.now(),
                  onDaySelected: (s, f) {
                    setState(() {
                      _focusedDay = DateUtils.dateOnly(f);
                      getLessonsToday();
                    });
                  },
                  onPageChanged: (f) {
                    _focusedDay = DateUtils.dateOnly(f);
                  },
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: DraggableScrollableSheet(
                  snap: true,
                  initialChildSize: 0.3,
                  minChildSize: 0.3,
                  maxChildSize: 1,
                  builder: (BuildContext context, myscrollController) {
                    return Container(
                      decoration: const BoxDecoration(
                        color: consts.misisWhite,
                        borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
                        boxShadow: [BoxShadow(
                            spreadRadius: -10,
                            blurRadius: 28,
                            color: const Color(0x31000000)
                        )],
                      ),
                      padding: const EdgeInsets.only(left: 10, top: 10, right: 10),
                      child: events.containsKey(_focusedDay)? ListView.builder(
                        controller: myscrollController,
                        itemCount: events[_focusedDay]!.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ListTile(
                            onTap: () async {
                              Navigator.pushNamed(context, '/RoomInfoPage', arguments: events[_focusedDay]![index]).then((value) {setState(() {}); getLessonsToday();});
                            },
                            title: Text(events[_focusedDay]![index].room.toString(),),
                            subtitle: Text(('Пара №${(events[_focusedDay]![index].para+1).toString()}'))
                          );
                        },
                      ) : Center(
                        child: Text('Пока пусто', style: TextStyle(color: Colors.grey),),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ),
        Container(
          color: consts.misisWhite,
          height: 100,
          width: double.infinity,
          padding: const EdgeInsets.all(10),
          child: ElevatedButton(
            onPressed: () { _showBottomModal(context); },
            child: const Text('Забронировать'),
          ),
        )
      ],
    );
  }
}

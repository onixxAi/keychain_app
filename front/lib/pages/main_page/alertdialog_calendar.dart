import 'package:flutter/material.dart';
import 'package:qr_app/data/key_info.dart';
import 'package:qr_app/data/key_info.dart';
import 'package:qr_app/utils/booking_utils.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import '../../consts.dart' as consts;

class AlertDialogCalendar extends StatefulWidget {
  AlertCalendarController alertCalendarController;
  AlertDialogCalendar(this.alertCalendarController, {Key? key})
      : super(key: key);

  @override
  State<AlertDialogCalendar> createState() => _AlertDialogCalendarState();
}

class _AlertDialogCalendarState extends State<AlertDialogCalendar> {
  Map<DateTime, List<BookingUnit>> events = {};

  void initState() {
    var box = Hive.box<BookingUnit>('booking');
    var mRoom = widget.alertCalendarController.roomName;
    events = BookingBoxUtils.listBookedDaysByRoom(box, mRoom);
  }

  List<BookingUnit> _getEventsForDay(DateTime day) {
    return events[DateUtils.dateOnly(day)] ?? [];
  }

  void selectDay(DateTime selected, DateTime focused) {
    setState(() {
      widget.alertCalendarController.dateTime = DateUtils.dateOnly(focused);
    });

    widget.alertCalendarController._bookingUnits =
        events[DateUtils.dateOnly(focused)] ?? [];
  }

  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: ListBody(
        children: <Widget>[
          SizedBox(
            width: double.maxFinite,
            child: TableCalendar(
              locale: 'ru_RU',
              headerStyle: const HeaderStyle(formatButtonVisible: false),
              firstDay: DateTime.now(),
              lastDay: DateTime.utc(2030, 3, 14),
              currentDay: widget.alertCalendarController.dateTime,
              focusedDay: DateTime.now(),
              onDaySelected: selectDay,
              onPageChanged: (f) {
                widget.alertCalendarController.dateTime = DateUtils.dateOnly(f);
              },
              calendarStyle: const CalendarStyle(
                markerDecoration: BoxDecoration(color: consts.misisBlue),
              ),
              eventLoader: (day) {
                return _getEventsForDay(day);
              },
            ),
          )
        ],
      ),
    );
  }
}

class AlertCalendarController extends ChangeNotifier {
  String _roomName;
  late DateTime _dateTime;
  late List<BookingUnit> _bookingUnits;

  List<BookingUnit> get bookingUnits => _bookingUnits;
  set bookingUnit(list) {
    _bookingUnits = list;
  }

  DateTime get dateTime => _dateTime;
  set dateTime(dateTime) {
    _dateTime = DateUtils.dateOnly(dateTime);
    notifyListeners();
  }

  String get roomName => _roomName;
  set roomName(roomName) {
    _roomName = roomName;
    notifyListeners();
  }

  AlertCalendarController(
      {String roomName = 'none', required DateTime dateTime})
      : _roomName = roomName,
        _dateTime = dateTime;
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'key_info.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BookingUnitAdapter extends TypeAdapter<BookingUnit> {
  @override
  final int typeId = 1;

  @override
  BookingUnit read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BookingUnit(
      fields[0] as String,
      fields[1] as DateTime,
      fields[2] as int,
    );
  }

  @override
  void write(BinaryWriter writer, BookingUnit obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.room)
      ..writeByte(1)
      ..write(obj.dateTime)
      ..writeByte(2)
      ..write(obj.para);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookingUnitAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

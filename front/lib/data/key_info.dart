import 'package:hive/hive.dart';

part 'key_info.g.dart';

@HiveType(typeId: 1)
class BookingUnit extends HiveObject {
  @HiveField(0)
  final String room;

  @HiveField(1)
  final DateTime dateTime;

  @HiveField(2)
  final int para;

  BookingUnit(this.room, this.dateTime, this.para);
}

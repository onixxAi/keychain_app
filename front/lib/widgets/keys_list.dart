import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_app/data/key_info.dart';
import 'package:intl/intl.dart';

import '../consts.dart' as consts;

class KeysList extends StatelessWidget {
  final List<BookingUnit> keys;
  final InteractiveElement textElement;
  final DateFormat formatter = DateFormat('yyyy-MM-dd');

  KeysList({Key? key, required this.keys, required this.textElement})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: keys.length,
        itemBuilder: (context, i) {
          return Column(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 10),
                color: consts.misisGray,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Row(
                        children: [
                          Text(
                            keys[i].room,
                            style: consts.misisHeaderBlack24Pt,
                          ),
                          Text(
                              '(${formatter.format(keys[i].dateTime)}-${keys[i].para} пара)',
                              overflow: TextOverflow.fade,
                              style: consts.misisTextDarkGray14Pt)
                        ],
                      ),
                    ),
                    Align(
                        alignment: Alignment.centerRight,
                        child: TextButton(
                          onPressed: () {
                            textElement.onPressed(i);
                          },
                          child: Text(textElement.text,
                              style: TextStyle(
                                  fontSize: 14, color: textElement.color)),
                        ))
                  ],
                ),
              ),
              const SizedBox(height: 10),
            ],
          );
        });
  }
}

class InteractiveElement {
  final String text;
  final Function(int i) onPressed;
  final Color color;

  const InteractiveElement(
      {required this.text, required this.onPressed, required this.color});
}
